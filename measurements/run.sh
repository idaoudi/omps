#!/bin/bash

SOMP_DIR=..
TRACES_DIR=traces
PLAT_DIR=../examples_platform

architecture=$1

algorithm=$2

echo $algorithm on $architecture

mSize=16384
models=(gemm comm commcache)
affinity=close

if [[ "$algorithm" = "cholesky" ]]
then
    bSize=512
    export SOMP_OVERLAP=1
    export SOMP_CACHELOCK=1
fi

if [[ "$algorithm" = "qr" ]]
then
    bSize=256
    export SOMP_OVERLAP=0
    export SOMP_CACHELOCK=0
fi

if [[ "$architecture" = "intel" ]]
then
    nCores=$(seq 1 36)
    machine=intel_xeon_gold_6240_cascadelake.xml
    export SOMP_ARCH=intel
    export SOMP_CPS=18
fi

if [[ "$architecture" = "amd" ]]
then
    nCores=$(seq 1 64)
    machine=amd2.xml
    export SOMP_ARCH=amd
    export SOMP_CPS=8
fi

for t in ${nCores[*]}
do
    echo $t cores...
    for model in ${models[*]}
    do
   $SOMP_DIR/somp $PLAT_DIR/$machine $TRACES_DIR/trace_${architecture}_${algorithm}_${mSize}_${bSize}.rec -m $mSize -b $bSize -t $t -o $model -a $affinity >> simulated_times_${architecture}_${algorithm}_${model}_${mSize}_${bSize}.dat 2> /dev/null &
    done
    wait
done





**sOMP**

sOMP is a simulator for task-based applications running on shared-memory architectures (check the [Wiki page](https://gitlab.inria.fr/idaoudi/omps/-/wikis/home) for more informations). 
This tool provides various models to refine the simulations precision (explicited in Usage section).

For architecture modeling, some Python3.8 scripts are available in the examples_platform/generator/ directory. Please use the "new_generator.py" script for now. You will still need to add some informations by hand. Some platforms are already set up and available in the examples_platform/ directory.

Traces can be generated by the [TiKKi](https://gitlab.inria.fr/openmp/tikki/-/wikis/home) tool or with the StarPU runtime. The format is .rec(GNU Recutils) and some examples are available in the /examples_trace/ directory.

**Compilation**:

*  add corresponding paths for SimGrid and TinyXML2 in the Makefile
*  make

**Usage**:

*  ./somp path/to/platform/file.xml path/to/trace/file.rec [options]
*  Mandatory options:
    *  -m : Matrix size (m*m matrix)
    *  -b : Bloc/tile size (b*b tile)
    *  -t : Number of threads/cores
    *  -o : Model options:
        *  nomodel      : no model is used by the simulator
        *  task         : task times based model
        *  comm         : communications based model
        *  commcache    : communications and L3 cache modelisation based model
    *  -a : Affinity (placement policy):
        *  close 
        *  spread
* You also need to set up sOMP environment variables: check out the "exports_somp" file

**Example**:

source exports_somp

./somp examples_platform/amd2.xml examples_trace/tasks_chol.rec -m 16384 -b 512 -t 36 -o commcache -a close



import sys

def difference(a,b):
    return a-b;

nat1  = []
nat2  = []
nat3  = []
nat1b = []
nat2b = []
nat3b = []
diff1 = []
diff2 = []
diff3 = []
nth   = []
i     = 0

if (int(sys.argv[1]) == 1):
    for line in open(sys.argv[2], 'r'):
        a, b = line.split()
        nat1.append(float(b))
        i += 1
        nth.append(i)
    for line in open(sys.argv[3], 'r'):
        a, b = line.split()
        nat1b.append(float(b))
    for i in range(len(nth)):
        diff1.append(difference(nat1[i], nat1b[i]))
    l = []
    for i in range(len(nth)):
        l = [nth[i], diff1[i]]
        print(*l)
        l = []

elif (int(sys.argv[1]) == 2):
    for line in open(sys.argv[2], 'r'):
        a, b, c = line.split()
        nat1.append(float(b))
        nat2.append(float(c))
        i += 1
        nth.append(i)
    for line in open(sys.argv[3], 'r'):
        a, b, c = line.split()
        nat1b.append(float(b))
        nat2b.append(float(c))
    for i in range(len(nth)):
        diff1.append(difference(nat1[i], nat1b[i]))
        diff2.append(difference(nat2[i], nat2b[i]))
    l = []
    for i in range(len(nth)):
        l = [nth[i], diff1[i], diff2[i]]
        print(*l)
        l = []

elif (int(sys.argv[1]) == 3):
    for line in open(sys.argv[2], 'r'):
        a, b, c, d = line.split()
        nat1.append(float(b))
        nat2.append(float(c))
        nat3.append(float(d))
        i += 1
        nth.append(i)
    for line in open(sys.argv[3], 'r'):
        a, b, c, d = line.split()
        nat1b.append(float(b))
        nat2b.append(float(c))
        nat3b.append(float(d))
    for i in range(len(nth)):
        diff1.append(difference(nat1[i], nat1b[i]))
        diff2.append(difference(nat2[i], nat2b[i]))
        diff3.append(difference(nat3[i], nat3b[i]))
    l = []
    for i in range(len(nth)):
        l = [nth[i], diff1[i], diff2[i], diff3[i]]
        print(*l)
        l = []








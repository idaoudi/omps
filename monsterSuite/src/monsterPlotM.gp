#gnuplotfile

#Arguments mapping:
    #ARG1 matrix size
    #ARG2 number of threads
    #ARG3 number of curves
    #ARG4 Chameleon file
    #ARG5 Kastors file
    #ARG6 number of models
    #ARG7 tile size 1
    #ARG8 tile size 2
    #ARG9 tile size 3

#Title
set title "AMD EPYC 7452 - Matrix size = ".ARG1

#Axis labels
set xlabel "Number of cores"
set ylabel "Precision error (%)"
#set ylabel "Time (s)"

#Axis ranges
set xrange[1:ARG2]
set yrange[-50:50]

#Misc
set key top left
set style data linespoints
set term postscript eps color solid font ",22"

#Output file FIXME
fileName   = "amdPrecModels"
fileVar    = ARG7.ARG8.ARG9
set output fileName.ARG1."-".fileVar.".eps"

#Plot
chamFile = ARG4
kasFile  = ARG5

if (ARG6 == 2){
    if (strlen(chamFile)>2 && strlen(kasFile)>2){
        plot chamFile using 1:2 title 'Cham. COMM - '.ARG7 lw 4 lc rgb "blue" ps 2, chamFile using 1:3 title 'Cham. NOMODEL - '.ARG7 lw 4 lc rgb "blue" ps 2, kasFile using 1:2 title 'Kas. COMM - '.ARG7 lw 4 lc rgb "green" ps 2, kasFile using 1:3 title 'Kas. NOMODEL - '.ARG7 lw 4 lc rgb "green" ps 2
    } 
    if (strlen(chamFile)>2 && strlen(kasFile)<3){
        plot chamFile using 1:2 title 'Cham. COMM - '.ARG7 lw 4 lc rgb "blue" ps 2, chamFile using 1:3 title 'Cham. NOMODEL - '.ARG7 lw 4 lc rgb "blue" ps 2
    }
    if (strlen(kasFile)>2 && strlen(chamFile)<3){
        plot kasFile using 1:2 title 'Kas. COMM - '.ARG7 lw 4 lc rgb "green" ps 2, kasFile using 1:3 title 'Kas. NOMODEL - '.ARG7 lw 4 lc rgb "green" ps 2
    }    
}

if (ARG6 == 3){
    if (strlen(chamFile)>2 && strlen(kasFile)>2){
        plot chamFile using 1:2 title 'Cham. COMM - '.ARG7 lw 4 lc rgb "blue" ps 2, chamFile using 1:3 title 'Cham. NOMODEL - '.ARG7 lw 4 lc rgb "blue" ps 2, chamFile using 1:4 title 'Cham. COMM+CACHE - '.ARG7 lw 4 lc rgb "blue" ps 2, kasFile using 1:2 title 'Kas. COMM - '.ARG7 lw 4 lc rgb "green" ps 2, kasFile using 1:3 title 'Kas. NOMODEL - '.ARG7 lw 4 lc rgb "green" ps 2, kasFile using 1:4 title 'Kas. COMM+CACHE - '.ARG7 lw 4 lc rgb "green" ps 2,
    } 
    if (strlen(chamFile)>2 && strlen(kasFile)<3){
        plot chamFile using 1:2 title 'Cham. COMM - '.ARG7 lw 4 lc rgb "blue" ps 2, chamFile using 1:3 title 'Cham. NOMODEL - '.ARG7 lw 4 lc rgb "blue" ps 2, chamFile using 1:4 title 'Cham. COMM+CACHE - '.ARG7 lw 4 lc rgb "blue" ps 2
    }
    if (strlen(kasFile)>2 && strlen(chamFile)<3){
        plot kasFile using 1:2 title 'Kas. COMM - '.ARG7 lw 4 lc rgb "green" ps 2, kasFile using 1:3 title 'Kas. NOMODEL - '.ARG7 lw 4 lc rgb "green" ps 2, kasFile using 1:4 title 'Kas. COMM+CACHE - '.ARG7 lw 4 lc rgb "green" ps 2,

    }    
}

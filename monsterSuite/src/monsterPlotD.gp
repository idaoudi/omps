#gnuplotfile

#Arguments mapping:
#ARG1 matrix size
#ARG2 number of threads
#ARG3 number of curves
#ARG4 Chameleon file
#ARG4 tile size 1
#ARG5 tile size 2
#ARG6 tile size 3

#Title
set title "AMD EPYC 7452 - Matrix size = ".ARG1

#Axis labels
set xlabel "Number of cores"
set ylabel "Time (s)"

#Axis ranges
set xrange[1:ARG2]
set yrange[-1:1]

#Misc
set key top left
set style data linespoints
set term postscript eps color solid font ",22"

#Output file FIXME
fileName   = "amdTimeDiff"
fileVar    = ARG5.ARG6.ARG8
set output fileName.ARG1."-".fileVar.".eps"

#Plot
myFile = ARG4

if (ARG3 == 1){
    plot myFile using 1:2 title ARG5 lw 4 lc rgb "blue" ps 2
} 

if (ARG3 == 2){
    plot myFile using 1:2 title ARG5 lw 4 lc rgb "blue" ps 2, myFile using 1:3 title ARG6 lw 4 lc rgb "green" ps 2
}

if (ARG3 == 3){
    plot myFile using 1:2 title ARG5 lw 4 lc rgb "blue" ps 2, myFile using 1:3 title ARG6 lw 4 lc rgb "green" ps 2, myFile using 1:4 title ARG8 lw 4 lc rgb "magenta" ps 2
}

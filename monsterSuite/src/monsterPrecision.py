import sys

def pourcentage(fix,var):
    return 100 - var*100/fix

nat1 = []
nat2 = []
nat3 = []
sim1 = []
sim2 = []
sim3 = []
nth  = []
prec1= []
prec2= []
prec3= []
i    = 0

if int(sys.argv[2]) == 1:
    for line in open(sys.argv[1], 'r'):
        a, b = line.split()
        nat1.append(float(a))
        sim1.append(float(b))
        i += 1
        nth.append(i)
    for i in range(len(nth)):
        prec1.append(pourcentage(nat1[i],sim1[i]))
    l = []
    for i in range(len(nth)):
        l = [nth[i], prec1[i]]
        print(*l)
        l = []

elif int(sys.argv[2]) == 2:
    for line in open(sys.argv[1], 'r'):
        a, b, c, d = line.split()
        nat1.append(float(a))
        sim1.append(float(b))
        nat2.append(float(c))
        sim2.append(float(d))
        i += 1
        nth.append(i)
    for i in range(len(nth)):
        prec1.append(pourcentage(nat1[i],sim1[i]))
        prec2.append(pourcentage(nat2[i],sim2[i]))
    l = []
    for i in range(len(nth)):
        l = [nth[i], prec1[i], prec2[i]]
        print(*l)
        l = []

elif int(sys.argv[2]) == 3:
    for line in open(sys.argv[1], 'r'):
        a, b, c, d, e, f = line.split()
        nat1.append(float(a))
        sim1.append(float(b))
        nat2.append(float(c))
        sim2.append(float(d))
        nat3.append(float(e))
        sim3.append(float(f))
        i += 1
        nth.append(i)
    for i in range(len(nth)):
        prec1.append(pourcentage(nat1[i],sim1[i]))
        prec2.append(pourcentage(nat2[i],sim2[i]))
        prec3.append(pourcentage(nat3[i],sim3[i]))
    l = []
    for i in range(len(nth)):
        l = [nth[i], prec1[i], prec2[i], prec3[i]]
        print(*l)
        l = []



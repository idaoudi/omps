#gnuplotfile

#Arguments mapping:
    #ARG1 matrix size
    #ARG2 number of threads
    #ARG3 number of curves
    #ARG4 Chameleon file
    #ARG5 Kastors file
    #ARG6 tile size 1
    #ARG7 tile size 2
    #ARG8 tile size 3

#Title
set title "AMD EPYC 7452 - Matrix size = ".ARG1

#Axis labels
set xlabel "Number of cores"
set ylabel "Precision error (%)"
#set ylabel "Time (s)"

#Axis ranges
set xrange[1:ARG2]
set yrange[-50:50]

#Misc
set key top left
set style data linespoints
set term postscript eps color solid font ",22"

#Output file FIXME
fileName   = "amdPrec"
fileVar    = ARG6.ARG7.ARG8
set output fileName.ARG1."-".fileVar.".eps"

#Plot
chamFile = ARG4
kasFile  = ARG5

if (ARG3 == 1){
    if (strlen(chamFile)>2 && strlen(kasFile)>2){
        plot chamFile using 1:2 title 'Chameleon - '.ARG6 lw 4 lc rgb "blue" ps 2, kasFile using 1:2 title 'Kastors - '.ARG6 lw 4 lc rgb "green" ps 2,
    } 
    if (strlen(chamFile)>2 && strlen(kasFile)<3){
        plot chamFile using 1:2 title 'Chameleon - '.ARG6 lw 4 lc rgb "blue" ps 2
    }
    if (strlen(kasFile)>2 && strlen(chamFile)<3){
        plot kasFile using 1:2 title 'Kastors - '.ARG6 lw 4 lc rgb "green" ps 2
    }    
}

if (ARG3 == 2){
    if (strlen(chamFile)>2 && strlen(kasFile)>2){
        plot chamFile using 1:2 title 'Chameleon - '.ARG6 lw 4 lc rgb "blue" ps 2, chamFile using 1:3 title 'Chameleon - '.ARG7 lw 4 lc rgb "orange" ps 2, kasFile using 1:2 title 'Kastors - '.ARG6 lw 4 lc rgb "green" ps 2, kasFile using 1:3 title 'Kastors - '.ARG7 lw 4 lc rgb "red" ps 2
    } 
    if (strlen(chamFile)>2 && strlen(kasFile)<3){
        plot chamFile using 1:2 title 'Chameleon - '.ARG6 lw 4 lc rgb "blue" ps 2, chamFile using 1:3 title 'Chameleon - '.ARG7 lw 4 lc rgb "orange" ps 2
    }
    if (strlen(kasFile)>2 && strlen(chamFile)<3){
        plot kasFile using 1:2 title 'Kastors - '.ARG6 lw 4 lc rgb "green" ps 2, kasFile using 1:3 title 'Kastors - '.ARG7 lw 4 lc rgb "red" ps 2
    }    
}

if (ARG3 == 3){
    if (strlen(chamFile)>2 && strlen(kasFile)>2){
        plot chamFile using 1:2 title 'Chameleon - '.ARG6 lw 4 lc rgb "blue" ps 2, chamFile using 1:3 title 'Chameleon - '.ARG7 lw 4 lc rgb "orange" ps 2, chamFile using 1:4 title 'Chameleon - '.ARG8 lw 4 lc rgb "cyan" ps 2, kasFile using 1:2 title 'Kastors - '.ARG6 lw 4 lc rgb "green" ps 2, kasFile using 1:3 title 'Kastors - '.ARG7 lw 4 lc rgb "red" ps 2, kasFile using 1:4 title 'Kastors - '.ARG8 lw 4 lc rgb "brown" ps 2

    } 
    if (strlen(chamFile)>2 && strlen(kasFile)<3){
        plot chamFile using 1:2 title 'Chameleon - '.ARG6 lw 4 lc rgb "blue" ps 2, chamFile using 1:3 title 'Chameleon - '.ARG7 lw 4 lc rgb "orange" ps 2, chamFile using 1:4 title 'Chameleon - '.ARG8 lw 4 lc rgb "cyan" ps 2
    }
    if (strlen(kasFile)>2 && strlen(chamFile)<3){
        plot kasFile using 1:2 title 'Kastors - '.ARG6 lw 4 lc rgb "green" ps 2, kasFile using 1:3 title 'Kastors - '.ARG7 lw 4 lc rgb "red" ps 2, kasFile using 1:4 title 'Kastors - '.ARG8 lw 4 lc rgb "brown" ps 2
    }    
}

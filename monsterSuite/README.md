**Welcome to the sOMP Monster Suite.**

This suite is designed to test the sOMP simulator with 2 different benchmarks: 
the **[Chameleon](https://gitlab.inria.fr/solverstack/chameleon)** library and the **[KaStORS](https://gitlab.inria.fr/openmp/kastors)** suite.

The sOMP Monster Suite directly produces graphs to visualize the results, but also provides all measurements in the CSV format (even if there is no commas :)).

**Available graph formats**
- **a single sOMP model**, various **tile**, matrix and thread sizes --> precision graphs to compare sOMP precision by tile size
- **a single tile size**, various **models**, matrix and thread sizes --> precision graphs to compare sOMP precision by sOMP models
- time graphs to compare **native execution times** for various matrix, tile and thread sizes (Chameleon vs Kastors)

**Configuring the sOMP Monster**

All configurations have to be made in the sOMPMonster file.

TODO.

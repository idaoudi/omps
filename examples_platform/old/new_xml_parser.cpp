#include <tinyxml2.h>
#include <string>
#include <iostream>

#ifndef XMLCheckResult
	#define XMLCheckResult(a_eResult) if (a_eResult != tinyxml2::XML_SUCCESS) { printf("Error: %i\n", a_eResult); return a_eResult; }
#endif

int main(){
    
    tinyxml2::XMLDocument doc;
    tinyxml2::XMLError eResult = doc.LoadFile("modified_platform.xml");
    XMLCheckResult(eResult);

    tinyxml2::XMLNode * pRoot = doc.FirstChild();

    tinyxml2::XMLElement * pElement = pRoot->FirstChildElement("zone");
    if (pElement == nullptr)
        std::cout << "Zone element not found! Check your XML platform file." << std::endl;

    const char* szAttributeText = nullptr;
    std::string id;
    
    tinyxml2::XMLElement * pElement2 = pElement->FirstChildElement("host");
    
    szAttributeText = pElement2->Attribute("id");
    if (szAttributeText == nullptr) 
        std::cout << "Host element not found! Check your XML platform file." << std::endl;
    
    id = szAttributeText;
    
    std::cout << id << std::endl;
}
